<?php

namespace Drupal\betternone;


use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class BetterNoneAlterer {

  use StringTranslationTrait;

  const POSITION_REMOVE = 'remove';
  const POSITION_FIRST = 'first';
  const POSITION_LAST = 'last';

  /**
   * @var string
   */
  protected $position;

  /**
   * @var string|\Drupal\Core\StringTranslation\TranslatableMarkup
   */
  protected $label;

  /**
   * BetterNoneAlterer constructor.
   *
   * @param string $position
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|string $label
   */
  public function __construct(string $position, $label) {
    $this->position = $position;
    $this->label = $label;
  }


  public static function fromFieldDefinitionAndWidget(FieldDefinitionInterface $fieldDefinition, WidgetInterface $widget) {
    // Default to show "none" only on single-valued, non-required fields.
    $position = ($fieldDefinition->getFieldStorageDefinition()->isMultiple()
      || $fieldDefinition->isRequired()) ? self::POSITION_REMOVE : self::POSITION_FIRST;
    $label = '';
    if ($widget) {
      $position = $widget->getThirdPartySetting('betternone', 'position', $position);
      $label = $widget->getThirdPartySetting('betternone', 'label', $label);
    }
    return new static($position, $label);
  }

  public function alter(array &$options) {
    // @sew https://www.drupal.org/project/drupal/issues/1585930
    $label = $this->label ?: $options[''] ?? $options['_none'] ?? $this->t('- None -');
    unset($options['']);
    unset($options['_none']);
    if ($this->position === self::POSITION_FIRST) {
      $options = ['_none' => $label] + $options;
    }
    elseif ($this->position === self::POSITION_LAST) {
      $options['_none'] = $label;
    }
  }

  public function settingsForm() {
    $form['#type'] = 'fieldset';
    $form['#title'] = $this->t('Better None Option');
    $form['position'] = [
      '#type' => 'select',
      '#title' => $this->t('Position'),
      '#options' => $this->getPositionOptions(),
      '#default_value' => $this->position,
    ];
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label override'),
      '#description' => $this->t('Leave empty to not change the default.'),
      '#default_value' => $this->label,
    ];
    return $form;
  }

  private function getPositionOptions() {
    return [
      self::POSITION_FIRST => $this->t('First'),
      self::POSITION_LAST => $this->t('Last'),
      self::POSITION_REMOVE => $this->t('Remove'),
    ];
  }

  public function alterSummary(array &$summary, WidgetInterface $widget) {
    if ($position = $widget->getThirdPartySetting('betternone', 'position')) {
      $summary[] = t('None-option position: @position', ['@position' => $this->getPositionOptions()[$position]]);
    }
    if ($label = $widget->getThirdPartySetting('betternone', 'label')) {
      $summary[] = t('None-option label: @label', ['@label' => $label]);
    }
  }

}
